package com.foxtrapp.kotlintest.presentation.managers

import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import com.foxtrapp.kotlintest.R
import com.foxtrapp.kotlintest.presentation.basics.BaseActivity
import com.foxtrapp.kotlintest.presentation.basics.BaseFragment
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */

class ToolbarManager @Inject constructor() {

    private var mToolbar: Toolbar? = null
    private var elevation: Float = 0.toFloat()

    fun register(_activity: BaseActivity) {
        mToolbar = _activity.findViewById(_activity.getToolbarId())
        if (mToolbar == null) return

        elevation = mToolbar!!.elevation
        _activity.setSupportActionBar(mToolbar)
    }

    fun register(view: View, baseFragment: BaseFragment) {
        mToolbar = view.findViewById(baseFragment.getToolbarId())
        if (mToolbar == null) return

        elevation = mToolbar!!.elevation
        (baseFragment.activity as BaseActivity).setSupportActionBar(mToolbar)
    }

    fun getToolbar(): Toolbar? {
        return mToolbar
    }

    fun showBackButton(_activity: BaseActivity) {
        if (_activity.supportActionBar == null)
            return
        _activity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        _activity.supportActionBar?.setHomeButtonEnabled(true)
        _activity.supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    fun hideBackBtn(_activity: BaseActivity) {
        if (_activity.supportActionBar == null)
            return
        _activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        _activity.supportActionBar?.setHomeButtonEnabled(false)
        _activity.supportActionBar?.setDisplayShowHomeEnabled(false)
    }

    fun setTitle(_message: String) {
        mToolbar!!.title = _message
    }

    fun showDefaultElevation() {
        mToolbar!!.elevation = elevation
    }

    fun hideElevation() {
        mToolbar!!.elevation = 0f
    }

    fun hideTitle(_activity: BaseActivity) {
        if (_activity.supportActionBar == null)
            return
        _activity.supportActionBar?.setDisplayShowTitleEnabled(false)
    }
}