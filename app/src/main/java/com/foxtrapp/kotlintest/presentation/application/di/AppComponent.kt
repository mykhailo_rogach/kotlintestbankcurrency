package com.foxtrapp.kotlintest.presentation.application.di

import android.app.Application
import com.foxtrapp.kotlintest.presentation.application.KotlinTestApp
import com.foxtrapp.kotlintest.presentation.basics.di.BaseActivityProvider
import com.foxtrapp.kotlintest.presentation.fragments.banks_list.di.BanksListProvider
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by
 * Mykhailo on 3/20/2018.
 */

@Singleton
@Component(modules = [(AppModule::class), (AndroidSupportInjectionModule::class), (BanksListProvider::class), (BaseActivityProvider::class)])
interface AppComponent : AndroidInjector<KotlinTestApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}