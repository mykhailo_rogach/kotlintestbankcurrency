package com.foxtrapp.kotlintest.presentation.basics

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
interface BasePresenter<in V> {
    fun onStop()
    fun setView(_view: V)
    fun destroyView()
}
