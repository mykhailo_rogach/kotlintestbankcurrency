package com.foxtrapp.kotlintest.presentation.managers

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.View
import com.foxtrapp.kotlintest.presentation.basics.BaseFragment
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */

class FragmentNavigator @Inject constructor() {

    //_____________________ Private variables ____________________//
    private var mFragmentManager: FragmentManager? = null
    private var mContainerId: Int = 0

    /**
     * The method register this class in your activity or fragment
     * @param fragmentManager - fragment manager or child fragment manager
     * @param containerId - id container for fragments
     */
    fun register(fragmentManager: FragmentManager, containerId: Int) {
        this.mFragmentManager = fragmentManager
        this.mContainerId = containerId
    }

    fun unRegister() {
        this.mFragmentManager = null
    }

    fun getContainerId(): Int {
        return mContainerId
    }

    /**
     * The method clear all fragments in fragment manager
     */
    fun clearAllFragments() {
        val entryCount = mFragmentManager?.backStackEntryCount
        if (entryCount != null) {
            if (entryCount <= 0)
                return
        }

        val entry = mFragmentManager!!.getBackStackEntryAt(0)
        val id = entry.id
        mFragmentManager?.popBackStackImmediate(id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun clearBackStackToFragmentOrShow(_baseFragment: BaseFragment) {
        val entryCount = mFragmentManager!!.backStackEntryCount
        val tag = _baseFragment.javaClass.toString()
        var searchedEntry: FragmentManager.BackStackEntry? = null
        for (i in 0 until entryCount) {
            val entry = mFragmentManager?.getBackStackEntryAt(i)
            if (entry?.name == tag) {
                searchedEntry = mFragmentManager?.getBackStackEntryAt(i)
                break
            }
        }

        if (searchedEntry != null)
            mFragmentManager?.popBackStackImmediate(searchedEntry.id, FragmentManager.POP_BACK_STACK_INCLUSIVE)

        showFragment(_baseFragment)
    }

    fun getFragmentManager(): FragmentManager? {
        return mFragmentManager
    }

    /**
     * The method return true - if back stack not empty and true another. And call back stack in fragment manager
     */
    fun popBackStack(): Boolean? {
        return mFragmentManager?.popBackStackImmediate()
    }

    /**
     * The method work replace fragment with back stack (key = class model fragment)
     * @param baseFragment - fragment for adding
     */
    fun showFragment(baseFragment: BaseFragment) {
        if (mFragmentManager != null)
            getTransaction()
                    ?.replace(mContainerId, baseFragment)
                    ?.addToBackStack(baseFragment.javaClass.toString())
                    ?.commit()
    }

    /**
     * The method work replace fragment without back stack
     * @param baseFragment - fragment for replacing
     */
    fun replaceFragment(baseFragment: BaseFragment) {
        if (mFragmentManager != null)
            getTransaction()
                    ?.replace(mContainerId, baseFragment)
                    ?.commit()
    }

    fun replaceFragmentWithLoss(baseFragment: BaseFragment) {
        if (mFragmentManager != null)
            getTransaction()
                    ?.replace(mContainerId, baseFragment)
                    ?.commitAllowingStateLoss()
    }

    fun addFragment(baseFragment: BaseFragment) {
        if (mFragmentManager != null)
            getTransaction()
                    ?.add(mContainerId, baseFragment)
                    ?.addToBackStack(baseFragment.javaClass.simpleName)
                    ?.commit()
    }

    fun showFragmentWithoutBackStack(baseFragment: BaseFragment) {
        if (mFragmentManager != null) {
            clearAllFragments()
            replaceFragment(baseFragment)
        }
    }

    fun getTopFragment(): BaseFragment {
        if (mFragmentManager == null)
            throw NullPointerException("FragmentManager in Navigator is null")

        return mFragmentManager?.findFragmentById(mContainerId) as BaseFragment
    }

    fun getFragmentCountInBackStack(): Int? {
        return mFragmentManager?.backStackEntryCount
    }

    /**
     * The method return fragment transaction
     */
    fun getTransaction(): FragmentTransaction? {
        return mFragmentManager?.beginTransaction()
    }
}