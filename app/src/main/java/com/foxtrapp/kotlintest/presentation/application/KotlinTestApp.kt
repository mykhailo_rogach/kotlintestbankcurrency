package com.foxtrapp.kotlintest.presentation.application

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import com.facebook.stetho.Stetho
import com.foxtrapp.kotlintest.presentation.application.di.DaggerAppComponent
import com.foxtrapp.kotlintest.presentation.managers.LoadBanksJobService
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import java.util.concurrent.TimeUnit

/**
 * Created by
 * Mykhailo on 3/20/2018.
 */
class KotlinTestApp : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        startLoadDataJobService()
    }

    override fun applicationInjector(): AndroidInjector<KotlinTestApp> {
        return DaggerAppComponent.builder().application(this).build()
    }

    private fun startLoadDataJobService() {
        val jobScheduler = applicationContext.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler

        jobScheduler.allPendingJobs.map {
            if (it.id == 1)
                return
        }
        val serviceName = ComponentName(applicationContext, LoadBanksJobService::class.java)
        val jobInfo = JobInfo.Builder(1, serviceName)
                .setPeriodic(TimeUnit.MINUTES.toMillis(30))
                .build()
        jobScheduler.schedule(jobInfo)
    }
}