package com.foxtrapp.kotlintest.presentation.fragments.banks_list

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import butterknife.BindView
import com.foxtrapp.kotlintest.R
import com.foxtrapp.kotlintest.data.models.organizations.BankListModel
import com.foxtrapp.kotlintest.presentation.basics.BaseFragment
import com.foxtrapp.kotlintest.presentation.fragments.banks_list.adapter.BanksAdapter
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
class BanksListFragment: BaseFragment(), IBanksListContract.IView {

    @BindView(R.id.rvBanks_FBL)
    lateinit var rvBanks: RecyclerView

    @Inject
    lateinit var mPresenter: IBanksListContract.IPresenter
    @Inject
    lateinit var mAdapter: BanksAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_banks_list)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setAdapter()
        mPresenter.setView(this)
    }

    private fun setAdapter() {
        rvBanks.adapter = mAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.destroyView()
    }

    override fun getToolbarId(): Int = 0

    override fun setToolbar() {

    }

    override fun showProgressLoading() {
    }

    override fun showBanks(banks: List<BankListModel>) {
        mAdapter.banks = banks
        mAdapter.notifyDataSetChanged()
    }

    override fun showError(_text: String?) {
        Toast.makeText(context, _text, LENGTH_LONG).show()
    }

}