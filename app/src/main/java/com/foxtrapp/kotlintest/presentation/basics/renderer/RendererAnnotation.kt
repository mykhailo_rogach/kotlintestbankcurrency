package com.foxtrapp.kotlintest.presentation.basics.renderer

import kotlin.reflect.KClass

/**
 * Created by
 * Mykhailo on 4/3/2018.
 */
@Target(AnnotationTarget.TYPE, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class RendererAnnotation(val stateClass: KClass<*>)