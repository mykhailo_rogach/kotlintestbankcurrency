package com.foxtrapp.kotlintest.presentation.fragments.banks_list.di

import android.support.v7.widget.RecyclerView
import com.foxtrapp.kotlintest.domain.banks_info.GetBanksInfoInteractor
import com.foxtrapp.kotlintest.domain.banks_info.IGetBanksInfoInteractor
import com.foxtrapp.kotlintest.presentation.basics.IBaseRenderer
import com.foxtrapp.kotlintest.presentation.fragments.banks_list.BanksListPresenter
import com.foxtrapp.kotlintest.presentation.fragments.banks_list.IBanksListContract
import com.foxtrapp.kotlintest.presentation.fragments.banks_list.adapter.BanksAdapter
import com.foxtrapp.kotlintest.presentation.fragments.banks_list.renderer.BanksListRenderer
import dagger.Module
import dagger.Provides

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */

@Module
class BanksListModule {

    @Provides
    fun provideInteractor(interactor: GetBanksInfoInteractor): IGetBanksInfoInteractor {
        return interactor
    }

    @Provides
    fun provideBanksListRenderer(): IBaseRenderer = BanksListRenderer()

    @Provides
    fun providePresenter(presenter: BanksListPresenter): IBanksListContract.IPresenter {
        return presenter
    }

    @Provides
    fun provideBankAdapter(): RecyclerView.Adapter<RecyclerView.ViewHolder> = BanksAdapter()
}