package com.foxtrapp.kotlintest.presentation.basics

import android.content.Context
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.Unbinder
import com.foxtrapp.kotlintest.presentation.managers.FragmentNavigator
import com.foxtrapp.kotlintest.presentation.managers.ToolbarManager
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */

abstract class BaseActivity: DaggerAppCompatActivity() {

    @Inject
    lateinit var mFragmentNavigator: FragmentNavigator

    @Inject
    lateinit var mToolbarManager: ToolbarManager

    lateinit var mUnBinder: Unbinder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerControllers()
    }

    override fun setContentView(@LayoutRes layoutResID: Int) {
        super.setContentView(layoutResID)
        mToolbarManager.register(this)
        mUnBinder = ButterKnife.bind(this)
    }

    override fun onDestroy() {
        mUnBinder.unbind()
        super.onDestroy()
        mFragmentNavigator.unRegister()
    }

    private fun registerControllers() {
        mFragmentNavigator.register(supportFragmentManager, getContainerId())
    }

    fun getFragmentNavigator(): FragmentNavigator? {
        return mFragmentNavigator
    }

    fun getToolbarManager(): ToolbarManager? {
        return mToolbarManager
    }

    @IdRes
    abstract fun getContainerId(): Int

    @IdRes
    abstract fun getToolbarId(): Int
}