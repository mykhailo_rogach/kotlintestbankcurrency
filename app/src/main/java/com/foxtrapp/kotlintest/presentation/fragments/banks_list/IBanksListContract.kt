package com.foxtrapp.kotlintest.presentation.fragments.banks_list

import com.foxtrapp.kotlintest.data.models.organizations.BankListModel
import com.foxtrapp.kotlintest.presentation.basics.BasePresenter
import com.foxtrapp.kotlintest.presentation.basics.BaseView

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
interface IBanksListContract {
    interface IView: BaseView {
        fun showProgressLoading()
        fun showBanks(banks: List<BankListModel>)
        fun showError(_text: String?)
    }
    interface IPresenter: BasePresenter<IView>
}