package com.foxtrapp.kotlintest.presentation.fragments.banks_list.renderer

import com.foxtrapp.kotlintest.domain.banks_info.state.IStateGetBanksInfo
import com.foxtrapp.kotlintest.domain.basics.IBaseState
import com.foxtrapp.kotlintest.presentation.basics.IBaseRenderer
import com.foxtrapp.kotlintest.presentation.basics.renderer.RendererAnnotation
import com.foxtrapp.kotlintest.presentation.fragments.banks_list.IBanksListContract
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 4/4/2018.
 */

@RendererAnnotation(stateClass = IStateGetBanksInfo.BanksInfoLoaded::class)
class BanksListRenderer @Inject constructor(): IBaseRenderer {

    override fun render(_state: IBaseState, _view: IBanksListContract.IView?) {
        val state: IStateGetBanksInfo.BanksInfoLoaded = _state as IStateGetBanksInfo.BanksInfoLoaded

        if (state.isLoading) {
            _view?.showProgressLoading()
            return
        }

        if (state.throwable != null) {
            _view?.showError(state.throwable.message)
            return
        }

        if (!state.banks.orEmpty().isEmpty()) {
            _view?.showBanks(state.banks.orEmpty())
        }
    }
}