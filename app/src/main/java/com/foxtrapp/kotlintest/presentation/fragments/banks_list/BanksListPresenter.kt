package com.foxtrapp.kotlintest.presentation.fragments.banks_list

import com.foxtrapp.kotlintest.domain.banks_info.IGetBanksInfoInteractor
import com.foxtrapp.kotlintest.presentation.basics.IBaseRenderer
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
class BanksListPresenter @Inject constructor(private val interactor: IGetBanksInfoInteractor,
                                             private val renderer: IBaseRenderer) : IBanksListContract.IPresenter {

    private var mView: IBanksListContract.IView? = null
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onStop() {

    }

    override fun setView(_view: IBanksListContract.IView) {
        mView = _view
        setUpStream()
    }

    private fun setUpStream() {
        val getBanksInfo = interactor.getBanksInfo()
        compositeDisposable.addAll(getBanksInfo.subscribe { renderer.render(it, mView) })
    }

    override fun destroyView() {
        compositeDisposable.clear()
        mView = null
    }
}