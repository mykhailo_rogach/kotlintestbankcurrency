package com.foxtrapp.kotlintest.presentation.basics

import com.foxtrapp.kotlintest.domain.basics.IBaseState
import com.foxtrapp.kotlintest.presentation.fragments.banks_list.IBanksListContract

/**
 * Created by
 * Mykhailo on 4/3/2018.
 */
interface IBaseRenderer{
    fun render(_state: IBaseState, _view: IBanksListContract.IView?)
}