package com.foxtrapp.kotlintest.presentation.fragments.banks_list.di

import com.foxtrapp.kotlintest.presentation.fragments.banks_list.BanksListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector



/**
 * Created by
 * Mykhailo on 3/22/2018.
 */

@Module
abstract class BanksListProvider {

    @ContributesAndroidInjector(modules = [(BanksListModule::class)])
    internal abstract fun provideBanksListFragmentFactory(): BanksListFragment
}