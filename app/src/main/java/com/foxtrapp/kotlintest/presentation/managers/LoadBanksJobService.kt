package com.foxtrapp.kotlintest.presentation.managers

import android.app.job.JobParameters
import android.app.job.JobService
import com.foxtrapp.kotlintest.data.repository.IGetBanksRepository
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 3/29/2018.
 */

class LoadBanksJobService : JobService() {

    @Inject
    lateinit var iGetBanksRepository: IGetBanksRepository
    lateinit var disposable: Disposable

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()

    }

    override fun onStartJob(params: JobParameters?): Boolean {
        disposable = iGetBanksRepository.getFromNetworkAndWriteToDB()
                .onErrorReturn {
                    jobFinished(params, true)
                    emptyList()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ jobFinished(params, false) })
        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        disposable.dispose()
        return false
    }

}