package com.foxtrapp.kotlintest.presentation.managers

import android.content.SharedPreferences
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 3/30/2018.
 */

class SharedPreferenceManager @Inject constructor(private val sharedPreferences: SharedPreferences) {

    private lateinit var mEditor: SharedPreferences.Editor

    init {
        setSharedPreferences()
    }

    private fun setSharedPreferences() {
        mEditor = sharedPreferences.edit()
    }

    fun setFirstRun(isFirstRun: Boolean) {
        mEditor.putBoolean("isFirstRun", isFirstRun).apply()
    }

    fun isFirstRun(): Boolean? {
        return sharedPreferences.getBoolean("isFirstRun", true)
    }
}