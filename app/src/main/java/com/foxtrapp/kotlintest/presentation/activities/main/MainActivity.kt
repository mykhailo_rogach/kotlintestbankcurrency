package com.foxtrapp.kotlintest.presentation.activities.main

import android.os.Bundle
import com.foxtrapp.kotlintest.R
import com.foxtrapp.kotlintest.presentation.basics.BaseActivity
import com.foxtrapp.kotlintest.presentation.fragments.banks_list.BanksListFragment


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mFragmentNavigator.showFragment(BanksListFragment())
    }

    override fun getContainerId(): Int = R.id.flContainer_AM

    override fun getToolbarId(): Int = 0
}
