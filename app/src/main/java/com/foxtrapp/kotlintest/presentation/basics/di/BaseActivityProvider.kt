package com.foxtrapp.kotlintest.presentation.basics.di

import com.foxtrapp.kotlintest.presentation.activities.main.MainActivity
import com.foxtrapp.kotlintest.presentation.basics.BaseActivity
import com.foxtrapp.kotlintest.presentation.managers.LoadBanksJobService
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by
 * Mykhailo on 3/23/2018.
 */

@Module
abstract class BaseActivityProvider {

    @ContributesAndroidInjector()
    abstract fun bindBaseActivity(): BaseActivity

    @ContributesAndroidInjector()
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector()
    abstract fun bindLoadDataService(): LoadBanksJobService
}