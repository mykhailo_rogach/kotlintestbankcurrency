package com.foxtrapp.kotlintest.presentation.application.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.foxtrapp.kotlintest.data.api.client.KotlinTestRestClient
import com.foxtrapp.kotlintest.data.api.client.NetworkInterceptor
import com.foxtrapp.kotlintest.data.database.BankDatabase
import com.foxtrapp.kotlintest.data.repository.GetBanksRepository
import com.foxtrapp.kotlintest.data.repository.IGetBanksRepository
import com.foxtrapp.kotlintest.presentation.managers.NetworkManager
import com.foxtrapp.kotlintest.presentation.managers.SharedPreferenceManager
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import javax.inject.Singleton

/**
 * Created by
 * Mykhailo on 3/20/2018.
 */

@Module
class AppModule {

    @Singleton
    @Provides
    internal fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    fun provideGson(): Gson = Gson()

    @Singleton
    @Provides
    fun provideNetworkInterceptor(networkManager: NetworkManager, context: Context): Interceptor = NetworkInterceptor(networkManager, context)

    @Singleton
    @Provides
    fun provideRestClient(interceptor: Interceptor): KotlinTestRestClient = KotlinTestRestClient(interceptor)

    @Singleton
    @Provides
    fun banksInfoDB(context: Context): BankDatabase = BankDatabase.create(context)

    @Singleton
    @Provides
    fun provideRepository(repository: GetBanksRepository): IGetBanksRepository {
        return repository
    }

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Singleton
    @Provides
    fun provideSharedPreferenceManager(sharedPreferences: SharedPreferences): SharedPreferenceManager = SharedPreferenceManager(sharedPreferences)
}