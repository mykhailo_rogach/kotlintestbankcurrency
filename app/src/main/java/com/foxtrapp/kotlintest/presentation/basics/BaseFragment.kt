package com.foxtrapp.kotlintest.presentation.basics

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import butterknife.ButterKnife
import butterknife.Unbinder
import com.foxtrapp.kotlintest.presentation.managers.FragmentNavigator
import com.foxtrapp.kotlintest.presentation.managers.ToolbarManager
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
abstract class BaseFragment: DaggerFragment() {
    private var mLayoutRes = -1
    private var mFragmentNavigator: FragmentNavigator? = null
    lateinit var imm: InputMethodManager
    lateinit var baseActivity: BaseActivity
    lateinit var mUnBinder: Unbinder

    @Inject
    lateinit var mToolbarManager: ToolbarManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (mLayoutRes == -1)
            throw RuntimeException("You have to call setContentView!")

        return inflater.inflate(mLayoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mUnBinder = ButterKnife.bind(this, view)
        mToolbarManager.register(view, this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        baseActivity = activity as BaseActivity
        mFragmentNavigator = baseActivity.getFragmentNavigator()
        setListeners()
        setToolbar()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                mFragmentNavigator?.popBackStack()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        mUnBinder.unbind()
        super.onDestroyView()
        hideKeyBoard()
    }

    protected fun hideKeyBoard() {
        val view = baseActivity.currentFocus
        if (view != null) {
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    protected fun setListeners() {

    }

    protected fun setContentView(@LayoutRes _resLayout: Int) {
        mLayoutRes = _resLayout
    }

    protected fun getFragmentNavigator(): FragmentNavigator? {
        return mFragmentNavigator
    }

    fun getToolbarManager(): ToolbarManager? {
        return mToolbarManager
    }

    abstract fun setToolbar()

    abstract fun getToolbarId(): Int
}