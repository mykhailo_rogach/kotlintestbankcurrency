package com.foxtrapp.kotlintest.presentation.basics

/**
 * Created by
 * Mykhailo on 3/27/2018.
 */
interface IMapper<in From, out To> {
    fun map(data: From): To
}