package com.foxtrapp.kotlintest.presentation.fragments.banks_list.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.foxtrapp.kotlintest.R
import com.foxtrapp.kotlintest.data.models.organizations.BankListModel
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 4/2/2018.
 */
class BanksAdapter @Inject constructor() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var banks: List<BankListModel> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BanksHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_bank, parent, false))
    }

    override fun getItemCount(): Int = banks.size

    override fun onBindViewHolder(_holder: RecyclerView.ViewHolder, _position: Int) {
        val holder = _holder as BanksHolder
        val bankListModel = banks[_position]

        holder.tvName.text      = bankListModel.name
        holder.tvRegion.text    = bankListModel.region
        holder.tvCity.text      = bankListModel.city
        holder.tvPhone.text     = bankListModel.phone
        holder.tvAddress.text   = bankListModel.address
    }

    class BanksHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.tvName_IB)
        lateinit var tvName: TextView
        @BindView(R.id.tvRegion_IB)
        lateinit var tvRegion: TextView
        @BindView(R.id.tvCity_IB)
        lateinit var tvCity: TextView
        @BindView(R.id.tvPhone_IB)
        lateinit var tvPhone: TextView
        @BindView(R.id.tvAddress_IB)
        lateinit var tvAddress: TextView

        init {
            ButterKnife.bind(this, itemView)
        }
    }
}