package com.foxtrapp.kotlintest.data.database.dao

import android.arch.persistence.room.*
import com.foxtrapp.kotlintest.data.database.models.CurrencyName
import io.reactivex.Flowable

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Dao
interface CurrencyNameDao {

    @Insert
    fun insert(objList: List<CurrencyName>)

    @Insert
    fun insertCurrency(obj: CurrencyName)

    @Update
    fun update(objList: List<CurrencyName>)

    @Delete
    fun delete(objList: List<CurrencyName>)

    @Delete
    fun deleteCurrency(obj: CurrencyName)

    @Query("SELECT * FROM currency_name WHERE countryCode = :countryCode")
    fun getCurrencyByCountryCode(countryCode: String): Flowable<CurrencyName>

    @Query("SELECT * FROM currency_name")
    fun getCurrencies(): Flowable<CurrencyName>

    @Query("DELETE FROM currency_name")
    fun deleteCurrencyNamesTable()

}