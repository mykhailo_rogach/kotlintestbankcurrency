package com.foxtrapp.kotlintest.data.api.services

import com.foxtrapp.kotlintest.data.models.BankResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers

/**
 * Created by
 * Mykhailo on 3/20/2018.
 */
interface BankService {

    @GET("ru/public/currency-cash.json")
    @Headers("Content-Type: application/json")
    fun getInfo(): Observable<BankResponse>
}