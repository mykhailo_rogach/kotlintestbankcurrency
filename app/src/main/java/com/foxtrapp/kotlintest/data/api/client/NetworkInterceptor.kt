package com.foxtrapp.kotlintest.data.api.client

import android.content.Context
import com.foxtrapp.kotlintest.presentation.managers.NetworkManager
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by
 * Mykhailo on 3/20/2018.
 */

@Singleton
class NetworkInterceptor @Inject constructor(private val networkManager: NetworkManager, private val context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (networkManager.isNetworkAvailable(context))
            return chain.proceed(chain.request())
        else
            throw NetworkException("No Internet Connection")
    }
}