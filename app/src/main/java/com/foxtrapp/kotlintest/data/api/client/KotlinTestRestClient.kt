package com.foxtrapp.kotlintest.data.api.client

import com.foxtrapp.kotlintest.data.api.services.BankService
import okhttp3.Interceptor
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by
 * Mykhailo on 3/20/2018.
 */

class KotlinTestRestClient @Inject constructor(private val networkInterceptor: Interceptor) : BaseRestClient() {

    lateinit var bankService: BankService

    init {
        setUpRestClient()
    }

    override fun getNetworkInterceptor(): Interceptor {
        return networkInterceptor
    }

    private fun setUpRestClient() {
        val retrofit = getRetrofitBuilder().client(getOkHttpClient()).build()
        bankService = retrofit.create(BankService::class.java)
    }
}