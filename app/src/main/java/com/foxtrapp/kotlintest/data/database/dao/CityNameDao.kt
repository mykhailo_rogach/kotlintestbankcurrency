package com.foxtrapp.kotlintest.data.database.dao

import android.arch.persistence.room.*
import com.foxtrapp.kotlintest.data.database.models.CityName
import io.reactivex.Flowable

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Dao
interface CityNameDao {

    @Insert
    fun insert(objList: List<CityName>)

    @Insert
    fun insertCity(obj: CityName)

    @Update
    fun update(objList: List<CityName>)

    @Delete
    fun delete(objList: List<CityName>)

    @Delete
    fun deleteCity(obj: CityName)

    @Query("SELECT * FROM city_name WHERE id = :id")
    fun getCityById(id: String): CityName

    @Query("DELETE FROM city_name")
    fun deleteCityNamesTable()

    @Query("SELECT * FROM city_name")
    fun getCities(): Flowable<CityName>

}