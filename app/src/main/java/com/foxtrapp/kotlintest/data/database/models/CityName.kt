package com.foxtrapp.kotlintest.data.database.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Entity(tableName = "city_name")
class CityName constructor(@PrimaryKey val id: String, val cityName: String)