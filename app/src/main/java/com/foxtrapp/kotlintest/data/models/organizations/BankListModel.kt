package com.foxtrapp.kotlintest.data.models.organizations

import java.io.Serializable

/**
 * Created by
 * Mykhailo on 3/27/2018.
 */
class BankListModel constructor(var id: String? = null,
                                var name: String? = null,
                                var phone: String? = null,
                                var address: String? = null,
                                var region: String? = null,
                                var city: String? = null) : Serializable