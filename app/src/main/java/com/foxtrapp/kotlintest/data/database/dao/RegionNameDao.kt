package com.foxtrapp.kotlintest.data.database.dao

import android.arch.persistence.room.*
import com.foxtrapp.kotlintest.data.database.models.RegionName
import io.reactivex.Flowable

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Dao
interface RegionNameDao {

    @Insert
    fun insert(objList: List<RegionName>)

    @Insert
    fun insertRegion(obj: RegionName)

    @Update
    fun update(objList: List<RegionName>)

    @Delete
    fun delete(objList: List<RegionName>)

    @Delete
    fun deleteRegion(obj: RegionName)

    @Query("SELECT * FROM region_name WHERE id = :id")
    fun getRegionById(id: String): RegionName

    @Query("SELECT * FROM region_name")
    fun getRegions(): Flowable<RegionName>

    @Query("DELETE FROM region_name")
    fun deleteRegionsTable()

}