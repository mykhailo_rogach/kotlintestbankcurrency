package com.foxtrapp.kotlintest.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.foxtrapp.kotlintest.data.database.models.UpdateDate
import io.reactivex.Flowable

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Dao
interface UpdateDateDao {

    @Insert
    fun insert(obj: UpdateDate)

    @Delete
    fun delete(obj: UpdateDate)

    @Query("SELECT * FROM date")
    fun getLastUpdateDate(): Flowable<List<UpdateDate>>

}