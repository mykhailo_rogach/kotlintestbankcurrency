package com.foxtrapp.kotlintest.data.api.client

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by
 * Mykhailo on 3/20/2018.
 */
abstract class BaseRestClient {

    fun getRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
                .baseUrl("http://resources.finance.ua/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(Gson()))
    }

    fun getOkHttpClient() : OkHttpClient {
        return OkHttpClient.Builder()
                .addNetworkInterceptor(getNetworkInterceptor())
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addNetworkInterceptor(StethoInterceptor())
                .build()
    }

    abstract fun getNetworkInterceptor(): Interceptor
}