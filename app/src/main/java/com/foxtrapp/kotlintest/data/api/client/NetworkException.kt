package com.foxtrapp.kotlintest.data.api.client

import java.lang.RuntimeException

/**
 * Created by
 * Mykhailo on 3/20/2018.
 */
class NetworkException(message: String?) : RuntimeException(message)