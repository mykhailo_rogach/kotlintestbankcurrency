package com.foxtrapp.kotlintest.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.foxtrapp.kotlintest.data.database.dao.*
import com.foxtrapp.kotlintest.data.database.models.*

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Database(entities = [
    (CityName::class),
    (CurrencyName::class),
    (RegionName::class),
    (Currency::class),
    (Organization::class),
    (UpdateDate::class)],
        version = 1, exportSchema = false)
abstract class BankDatabase : RoomDatabase() {

    companion object {
        fun create(context: Context): BankDatabase {
            return Room.databaseBuilder(
                    context.applicationContext,
                    BankDatabase::class.java,
                    "banks.db").build()
        }
    }

    abstract fun getCityNameDao(): CityNameDao
    abstract fun getCurrencyNameDao(): CurrencyNameDao
    abstract fun getCurrencyDao(): CurrencyDao
    abstract fun getRegionNameDao(): RegionNameDao
    abstract fun getOrganizationDao(): OrganizationDao
    abstract fun getUpdateDateDao(): UpdateDateDao

}