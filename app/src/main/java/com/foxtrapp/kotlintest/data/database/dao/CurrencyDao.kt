package com.foxtrapp.kotlintest.data.database.dao

import android.arch.persistence.room.*
import com.foxtrapp.kotlintest.data.database.models.Currency
import io.reactivex.Flowable

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Dao
interface CurrencyDao {

    @Insert
    fun insert(objList: List<Currency>)

    @Insert
    fun insertCurrency(obj: Currency)

    @Update
    fun update(objList: List<Currency>)

    @Delete
    fun delete(objList: List<Currency>)

    @Delete
    fun deleteCurrency(obj: Currency)

    @Query("SELECT * FROM currency WHERE organizationId = :organizationId")
    fun getCurrenciesByOrganization(organizationId: String): Flowable<List<Currency>>

    @Query("DELETE FROM currency")
    fun deleteCurrencyTable()

}