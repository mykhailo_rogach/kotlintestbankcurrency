package com.foxtrapp.kotlintest.data.repository

import com.foxtrapp.kotlintest.data.api.client.KotlinTestRestClient
import com.foxtrapp.kotlintest.data.database.BankDatabase
import com.foxtrapp.kotlintest.data.database.models.*
import com.foxtrapp.kotlintest.data.database.models.Currency
import com.foxtrapp.kotlintest.data.models.organizations.BankListModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
class GetBanksRepository @Inject constructor(private val bankDatabase: BankDatabase, private val kotlinTestRestClient: KotlinTestRestClient) : IGetBanksRepository {

    override fun getBankInfo(): Observable<List<BankListModel>> {
        return getBanksFromDB()
                .mergeWith(getFromNetworkAndWriteToDB())
                .subscribeOn(Schedulers.io())
    }

    private fun getBanksFromDB(): Observable<List<BankListModel>> {
        return bankDatabase.getOrganizationDao().getOrganizations()
                .toObservable()
                .map {
                    val banks = mutableListOf<BankListModel>()
                    for (organization in it) {
                        val bankListModel = BankListModel().apply {
                            id = organization.id
                            name = organization.title
                            address = organization.address
                            phone = organization.phone
                        }
                        bankListModel.city = bankDatabase.getCityNameDao().getCityById(organization.cityId).cityName
                        bankListModel.region = bankDatabase.getRegionNameDao().getRegionById(organization.regionId).regionName
                        banks.add(bankListModel)
                    }
                    banks.toList()
                }
    }

    override fun getFromNetworkAndWriteToDB(): Observable<List<BankListModel>> {
        return kotlinTestRestClient.bankService.getInfo().flatMap {
            val currencies = mutableListOf<Currency>()
            val cities = mutableListOf<CityName>()
            val regions = mutableListOf<RegionName>()
            val currenciesNames = mutableListOf<CurrencyName>()
            val organizations = mutableListOf<Organization>()
            it.organizations.map { organization ->
                organizations.add(Organization(
                        id = organization.id,
                        title = organization.title,
                        phone = organization.phone,
                        address = organization.address,
                        orgType = organization.orgType,
                        regionId = organization.regionId,
                        cityId = organization.cityId))
                organization.currencies.forEach {
                    currencies.add(Currency(
                            UUID.randomUUID().mostSignificantBits,
                            organization.id,
                            it.key,
                            it.value.ask,
                            it.value.bid))
                }
            }

            it.cities.forEach { cities.add(CityName(it.key, it.value)) }
            it.regions.forEach { regions.add(RegionName(it.key, it.value)) }
            it.currencies.forEach { currenciesNames.add(CurrencyName(it.key, it.value)) }

            bankDatabase.getCityNameDao().deleteCityNamesTable()
            bankDatabase.getRegionNameDao().deleteRegionsTable()
            bankDatabase.getCurrencyNameDao().deleteCurrencyNamesTable()
            bankDatabase.getOrganizationDao().deleteOrganizationsTable()
            bankDatabase.getCurrencyDao().deleteCurrencyTable()

            bankDatabase.getCityNameDao().insert(cities)
            bankDatabase.getRegionNameDao().insert(regions)
            bankDatabase.getCurrencyNameDao().insert(currenciesNames)
            bankDatabase.getOrganizationDao().insert(organizations)
            bankDatabase.getCurrencyDao().insert(currencies)

            getBanksFromDB()
        }
                .subscribeOn(Schedulers.io())
    }
}