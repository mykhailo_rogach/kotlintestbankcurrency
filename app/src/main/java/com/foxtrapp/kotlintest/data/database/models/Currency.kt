package com.foxtrapp.kotlintest.data.database.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Entity(foreignKeys = [(ForeignKey(entity = Organization::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("organizationId"),
        onDelete = CASCADE))], tableName = "currency",
        indices = [(Index("organizationId"))])
data class Currency constructor(@PrimaryKey val id: Long, val organizationId: String, val countryCode: String, val ask: String, val bid: String)