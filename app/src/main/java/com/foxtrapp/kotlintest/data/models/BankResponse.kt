package com.foxtrapp.kotlintest.data.models

import com.foxtrapp.kotlintest.data.database.OrganizationResponse
import com.foxtrapp.kotlintest.data.database.models.Organization

/**
 * Created by
 * Mykhailo on 3/20/2018.
 */
data class BankResponse constructor(val date: String,
                                    val organizations: List<OrganizationResponse>,
                                    val orgTypes: HashMap<Int, String>,
                                    val currencies: HashMap<String, String>,
                                    val regions: HashMap<String, String>,
                                    val cities: HashMap<String, String>)