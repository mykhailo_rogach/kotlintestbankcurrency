package com.foxtrapp.kotlintest.data.database

import android.arch.persistence.room.PrimaryKey
import com.foxtrapp.kotlintest.data.database.models.Currency

/**
 * Created by
 * Mykhailo on 3/23/2018.
 */
class OrganizationResponse constructor(val id: String,
                                       val title: String,
                                       val phone: String,
                                       val address: String,
                                       val orgType: Int,
                                       val regionId: String,
                                       val cityId: String,
                                       val currencies: HashMap<String, Currency>) {
}