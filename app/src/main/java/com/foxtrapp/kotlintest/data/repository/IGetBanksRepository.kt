package com.foxtrapp.kotlintest.data.repository

import com.foxtrapp.kotlintest.data.models.organizations.BankListModel
import io.reactivex.Observable

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
interface IGetBanksRepository {
    fun getBankInfo(): Observable<List<BankListModel>>
    fun getFromNetworkAndWriteToDB(): Observable<List<BankListModel>>
}