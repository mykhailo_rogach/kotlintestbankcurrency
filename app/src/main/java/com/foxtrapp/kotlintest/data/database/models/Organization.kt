package com.foxtrapp.kotlintest.data.database.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Entity(foreignKeys = [
    (ForeignKey(entity = CityName::class, parentColumns = arrayOf("id"), childColumns = arrayOf("cityId"), onDelete = CASCADE)),
    (ForeignKey(entity = RegionName::class, parentColumns = arrayOf("id"), childColumns = arrayOf("regionId"), onDelete = CASCADE))],
        tableName = "organization",
        indices = [(Index("cityId")), (Index("regionId"))])
data class Organization constructor(@PrimaryKey val id: String,
                                    val title: String,
                                    val phone: String,
                                    val address: String,
                                    val orgType: Int,
                                    val regionId: String,
                                    val cityId: String)