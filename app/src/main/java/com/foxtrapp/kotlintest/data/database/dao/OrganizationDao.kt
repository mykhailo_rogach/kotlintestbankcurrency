package com.foxtrapp.kotlintest.data.database.dao

import android.arch.persistence.room.*
import com.foxtrapp.kotlintest.data.database.models.Organization
import io.reactivex.Flowable

/**
 * Created by
 * Mykhailo on 3/21/2018.
 */

@Dao
interface OrganizationDao {

    @Insert
    fun insert(objList: List<Organization>)

    @Insert
    fun insertOrganization(obj: Organization)

    @Update
    fun update(objList: List<Organization>)

    @Delete
    fun delete(objList: List<Organization>)

    @Delete
    fun deleteOrganization(obj: Organization)

    @Query("SELECT * FROM organization WHERE id = :id")
    fun getOrganizationById(id: String): Flowable<Organization>

    @Query("SELECT * FROM organization")
    fun getOrganizations(): Flowable<List<Organization>>

    @Query("DELETE FROM organization")
    fun deleteOrganizationsTable()
}