package com.foxtrapp.kotlintest.domain.banks_info

import com.foxtrapp.kotlintest.domain.banks_info.state.IStateGetBanksInfo
import io.reactivex.Observable

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
interface IGetBanksInfoInteractor {

    fun getBanksInfo(): Observable<IStateGetBanksInfo>
}