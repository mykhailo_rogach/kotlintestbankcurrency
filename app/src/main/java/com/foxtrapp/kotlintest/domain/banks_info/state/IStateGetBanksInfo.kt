package com.foxtrapp.kotlintest.domain.banks_info.state

import com.foxtrapp.kotlintest.data.models.organizations.BankListModel
import com.foxtrapp.kotlintest.domain.basics.IBaseState

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
interface IStateGetBanksInfo: IBaseState {

    data class BanksInfoLoaded (val isLoading: Boolean,
                                val throwable: Throwable?,
                                val banks: List<BankListModel>?): IStateGetBanksInfo {
        companion object {
            fun loading(): IStateGetBanksInfo = BanksInfoLoaded(true, null, null)
            fun error(throwable: Throwable?): IStateGetBanksInfo = BanksInfoLoaded(false, throwable, null)
            fun success(banks: List<BankListModel>?): IStateGetBanksInfo = BanksInfoLoaded(false, null, banks)
        }
    }
}