package com.foxtrapp.kotlintest.domain.banks_info

import com.foxtrapp.kotlintest.data.repository.IGetBanksRepository
import com.foxtrapp.kotlintest.domain.banks_info.state.IStateGetBanksInfo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by
 * Mykhailo on 3/22/2018.
 */
class GetBanksInfoInteractor @Inject constructor(private val iGetBanksRepository: IGetBanksRepository) : IGetBanksInfoInteractor {


    override fun getBanksInfo(): Observable<IStateGetBanksInfo> {
        return iGetBanksRepository.getBankInfo()
                .map { t -> IStateGetBanksInfo.BanksInfoLoaded.success(t) }
                .onErrorReturn { t -> IStateGetBanksInfo.BanksInfoLoaded.error(t) }
                .startWith(IStateGetBanksInfo.BanksInfoLoaded.loading())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}